(function (scope) {
    'use strict';
    scope.Config = {
        fileTypes: {
            document: ['doc', 'docx', 'pdf', 'txt'],
            image: ['jpeg', 'jpg', 'png', 'gif'],
            video: ['avi', 'mpg4', 'mkv'],
            audio: ['mp3', 'ogg', 'aac']
        }
    }

})(window);