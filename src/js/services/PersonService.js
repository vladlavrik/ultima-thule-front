(function () {
	'use strict';
	var app = angular.module('app');

	app.factory('Person', ['Api', '$q',
		function(Api, $q) {
			return {
				get: function () {
					return $q(function (resolve, reject) {
						Api.user.get().$promise.then(function (resp) {
							if(resp.error) {
								reject(resp.error);
								return;
							}
							resolve(resp.result);
						}).catch(function (err) {
							reject(err.statusText);
						});
					});
				}
			}
		}]);
})();