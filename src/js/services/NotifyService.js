(function () {
    'use strict';
    var app = angular.module('app');

    app.factory('Notify', [function() {


        function Emitter() {}

        Emitter.prototype = Object.create(Array);

        Emitter.prototype.emit = function (name) {
            var event = this[name];
            if(!event || !event.length) {
                return;
            }

            var args = Array.prototype.slice.call(arguments, 1);
            var i = 0;

            for(; i < event.length; i++) {
                event[i].apply(null, args);
            }
        };

        Emitter.prototype.add = function (name, fn) {
            this[name] = this[name] || [];
            this[name].push(fn);
        };

        Emitter.prototype.remove = function (name, fn) {
            if(!fn || !this[name]) {
                this[name] = [];
                return;
            }

            var pos = this[name].indexOf(fn);
            if (pos) {
                this.splice(pos, i);
            }
        };


        return {
            create: function () {
                return new Emitter();
            }
        };

        //todo test

    }]);
})();
