(function () {
	'use strict';
	var app = angular.module('app');

	app.factory('Upload', ['Notify', function(Notify) {

		var options = {
			method: 'POST',
			url: '/api/file'
		};

		var emitter = Notify.create();


		function onProgress(event) {
			emitter.emit('progress', event.loaded, event.total);
		}


		function onLoad() {
			try {
				var error = JSON.parse(this.responseText).error
			} catch (err) {
				emitter.emit('error', err);
			}

			if (this.status == 200 && error === null) {
				emitter.emit('success');
			} else {
				emitter.emit('error', error || this.statusText);
			}
		}


		function onError() {
			emitter.emit('error'); //todo args
		}


		return {

			on: emitter.add.bind(emitter),
			
			off: emitter.remove.bind(emitter),

			send: function (data) {
				var form = new FormData();
				var args = arguments;
				var i, e, cur, count = 0;

				for(i = 0; i < args.length; i++) {
					cur = args[i];

					// if FileList
					if(cur instanceof FileList) {
						for(e = 0; e < cur.length; e++) {
							form.append('file[]', cur[e]);
							count++;
						}
						continue;
					}

					// if text input
					if(cur.nodeName === 'INPUT') {
						form.append(cur.name, cur.value);
						continue;
					}

					// if object
					Object.keys(cur).forEach(function (item) {
						form.append(item, cur[item]);
					});
				}

				if(!count) {
					return;
				}

				var xhr = new XMLHttpRequest();

				xhr.upload.onprogress = onProgress;

				xhr.onload = onLoad;

				xhr.onerror = onError;

				xhr.open(options.method, options.url, true);

				xhr.send(form);

				emitter.emit('start');

			}

		};

		// todo front check size files, upload folders
	}]);
})();