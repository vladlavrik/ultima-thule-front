(function () {
	'use strict';
	const app = angular.module('app');

	app.factory('Audio', ['Notify',
		(Notify) => {

			let audio = new Audio();
			let emitter = Notify.create();
			let context = new AudioContext();
			let source = context.createMediaElementSource(audio);
			let processor = context.createScriptProcessor(2048, 2, 2);
			let analyser = context.createAnalyser();
			let bands;
			var frequencies = [50, 100, 200, 400, 800, 1500, 3000, 5000, 7000, 10000];
			var filters = null;

			let list = null;
			let shuffled = null;
			let current = null;

			let isShuffle = false;
			let isLoopList = false;
			let isLoopOne = false;
			let isVirtualization = false;
			let isEqualizer = false;

			let curTime;
			let curProgress;
			let curPreload;
			let options = {
				srcDir: '/',
				measure: 100
			};

			source.connect(context.destination);

			/*
			 * virtualization web audio api
			 */
			analyser.smoothingTimeConstant = 0.3;
			analyser.fftSize = 512;
			bands = new Uint8Array(analyser.frequencyBinCount);
			analyser.connect(processor);

			/*
			 * equalizer web audio api
			 */
			filters = frequencies.map(frequency => {
				let filter = context.createBiquadFilter();

				filter.type = 'peaking';
				filter.frequency.value = frequency;
				filter.Q.value = 1;
				filter.gain.value = 0;

				return filter;
			});

			filters.reduce((prev, curr) => {
				prev.connect(curr);
				return curr;
			});





			/*
			 * add audio element listeners
			 */
			audio.oncanplay = () => audio.play();


			audio.ontimeupdate = () => {
				let time = Math.round(audio.currentTime);
				let progress = Math.round(audio.currentTime / audio.duration * options.measure);
				if(time !== curTime || progress !== curProgress) {
					curTime = time;
					curProgress = progress;
					emitter.emit('progress', curProgress, curTime);
				}
			};


			audio.onprogress = () => {
				let preload = audio.buffered.length
					? audio.buffered.end(0) / audio.duration * options.measure
					: 0;
				if(preload !== curPreload) {
					curPreload = preload;
					emitter.emit('preload', preload);
				}
			};


			audio.onended = () => {
				if(isLoopOne) {
					audio.play();
					return true;
				}

				let playlist = isShuffle ? shuffled : list;
				let index = playlist.indexOf(current) + 1;

				if(!playlist[index] && (isShuffle || isLoopList)) {
					index = 0
				}
				if(!playlist[index]) {
					return;
				}

				current = playlist[index];
				audio.src = options.srcDir + current;

				emitter.emit('new', list.indexOf(current))
			};



			
			return {
				on: emitter.add.bind(emitter),

				off: emitter.remove.bind(emitter),

				setOptions (opts) {
					angular.extend(options, opts);
				},

				init (playlist) {
					list = playlist;
					audio.pause();
					audio.src = '';
					isShuffle = !!isShuffle; // update shuffle list
				},

				play (index) {
					current = list[index];
					audio.src = options.srcDir + current;
					isVirtualization = !!isVirtualization; // update virtualization listener
				},

				pause () {
					audio.paused
						? audio.play()
						: audio.pause();
					emitter.emit('paused', audio.paused);
				},

				setVolume (volume) {
					audio.volume = volume;
				},

				setPosition  (position, measure) {
					audio.currentTime = measure
						? position / measure * audio.duration
						: position;
				},

				destroy () {
					audio.pause();
					audio.src = '';
					list = null;
					shuffled = null;
					processor.onaudioprocess = null;
				},

				get loop() {
					return isLoopOne ? 'one' : isLoopList ? 'list' : false;
				},
				set loop(value) {
					isLoopOne = value === 'one';
					isLoopList = isLoopOne || value === 'list';
				},

				get shuffle() {
					return isShuffle;
				},
				set shuffle(value) {
					isShuffle = !!value;
					if (isShuffle) {
						shuffled = list.slice();
						let i = shuffled.length;
						while (--i) {
							let random = Math.floor(Math.random() * (i + 1));
							let temp = shuffled[i];
							shuffled[i] = shuffled[random];
							shuffled[random] = temp;
						}
					}
				},

				get virtualization() {
					return isVirtualization;
				},
				set virtualization(value) {
					isVirtualization = !!value;
					if (isVirtualization) {
						source.connect(analyser);
						processor.connect(context.destination);
						processor.onaudioprocess = () => {
							analyser.getByteFrequencyData(bands);
							emitter.emit('frequency', bands);
						};
					} else {
						processor.onaudioprocess = null;
						processor.disconnect(context.destination);
						source.disconnect(analyser);
					}
				},

				get equalizer() {
					return isEqualizer;
				},
				set equalizer(value) {
					if (value && !isEqualizer) {
						isEqualizer = true;
						source.connect(filters[0]);
						filters[filters.length - 1].connect(context.destination);
					}

					if (!value) {
						isEqualizer = false;
						// disconnect
						return;
					}

					value.forEach((val, i) => {
						filters[i].gain.value = val;
					});
				}

			};


			//todo destroy
			//todo play list, loop, repeat
			//todo: play error event, oncanplay use, preload next track
			//todo bug: on click pause preload progress forced do 100%
			//todo проверить, что быдет если попытатся проиграть невалидный формат, проверить надобность отбивки лишних событий
		}
	]);
})();