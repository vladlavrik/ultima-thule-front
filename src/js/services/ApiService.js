(function () {
	'use strict';
	var app = angular.module('app');

	app.factory('Api', ['$resource', function($resource) {

		return {
			signIn: $resource('/api/signin'),

			user: $resource('/api/user'),

			folder: $resource('/api/folder/:id/:section', {}, {update: {method: 'PUT'}}),

			file: $resource('/api/file/:id', {}, {update: {method: 'PUT'}}),

			files: $resource('/api/files/:parentId')
		};
	}]);
})();