(function () {
	'use strict';
	var app = angular.module('app', ['ui.router', 'ngResource']);

	app.constant('Config', Config);

	app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

		// init main states
		$stateProvider
			.state('auth', {
				url: '/auth/:view',
				params: {
					view: 'signin'
				},
				data: {
					requireLogin: false
				},
				views: {
					'window': {
						templateUrl: 'auth.html',
						controller: 'AuthCtrl'
					}
				}
			})
			.state('main', {
				abstract: true,
				data: {
					requireLogin: true
				},
				views: {
					'window': {
						templateUrl: 'page.html',
						controller: 'PageCtrl'
					}
				}
			})


			// init sections states
			.state('main.files', {
				url: '/files/:folderId',
				params: {
					folderId: {
						value: null,
						squash: true
					}
				},
				views: {
					'main': {
						templateUrl: 'files.html',
						controller: 'FilesCtrl'
					}
				}
			})
			.state('main.images', {
				url: '/images',
				views: {
					'main': {
						templateUrl: 'images.html',
						controller: 'ImagesCtrl'
					}
				}
			})
			.state('main.music', {
				url: '/music',
				views: {
					'main': {
						templateUrl: 'music.html',
						controller: 'MusicCtrl'
					}
				}
			})
			.state('main.videos', {
				url: '/videos',
				views: {
					'main': {
						templateUrl: 'videos.html',
						controller: 'VideosCtrl'
					}
				}
			})
			.state('main.documents', {
				url: '/documents',
				views: {
					'main': {
						templateUrl: 'documents.html',
						controller: 'DocumentsCtrl'
					}
				}
			});



		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});

		$urlRouterProvider.otherwise(function($injector){
			var $state = $injector.get("$state");
			$state.go('main.files');
		});

	});



	app.config(function($httpProvider) {
		//$httpProvider.interceptors.push(function ($timeout, $q, $injector) {
		//	var Person, $http, $state;
		//
		//	// this trick must be done so that we don't receive
		//	// `Uncaught Error: [$injector:cdep] Circular dependency found`
		//	$timeout(function () {
		//		Person = $injector.get('Person');
		//		$http = $injector.get('$http');
		//		$state = $injector.get('$state');
		//	});
		//
		//	return {
		//		responseError: function (rejection) {
		//			if (rejection.status !== 401) {
		//				return rejection;
		//			}
		//
		//			var deferred = $q.defer();
		//
		//			Person.get().$promise
		//					.then(function () {
		//						deferred.resolve( $http(rejection.config) );
		//					})
		//					.catch(function () {
		//						/* todo
		//							место редиректа вызвать уведомление о проблемы авторизации
		//							и предложения опвторной проверки или перехода на перелогин
		//							или редирект и уведомление
		//						  */
		//						$state.go('login');
		//						deferred.reject(rejection);
		//					});
		//
		//			return deferred.promise;
		//		}
		//	};
		//});
	});


	app.run(function ($rootScope, $state) {
		$rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
			var requireLogin = toState.data.requireLogin;
			if(!requireLogin || $rootScope.currentUser) {
				return;
			}
			event.preventDefault();

			// unauthorized redirect to auth page
			if($rootScope.currentUser === null) {
				$state.go('auth');
				return;
			}

			// wait auth response
			if($rootScope.currentUser === undefined) {
				var handler = function(event) {
					if(event.name === 'loginSuccess') {
						$state.go(toState.name, toParams);
					} else {
						$state.go('auth');
					}
					loginSuccessEvent();
					loginFailedEvent();
				};
				var loginSuccessEvent = $rootScope.$on('loginSuccess', handler);
				var loginFailedEvent = $rootScope.$on('loginFailed', handler);
			}
		});
	});

	//todo server error handlers

})();