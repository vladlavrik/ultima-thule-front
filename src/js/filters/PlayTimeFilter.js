(function () {
    'use strict';
    const app = angular.module('app');

    app.filter('playTime', () => {
        return t => {
            let s, m, h;
            s = Math.round(t);
            h = Math.floor(s / 3600);
            s -= h * 3600;

            m = Math.floor(s / 60);
            s -= m * 60;

            return '' +
                (h ? ('0' + h.toString()).slice(-2) + ':' : '') +
                ('0' + m.toString()).slice(-2)  + ':' +
                ('0' + s.toString()).slice(-2);
        }
    })
})();

