(function () {
	'use strict';
	var app = angular.module('app');

	app.filter('memorySize', function(){
		return function(size){
			var units = ['b', 'kb', 'mb', 'gb'];
			var i;

			for(i = 0; i < units.length; i++) {
				if(size < 1024 || i === units.length - 1) {
					return (size).toFixed(1) + ' ' + units[i];
				}
				size /= 1024;
			}
		}
	})
})();

