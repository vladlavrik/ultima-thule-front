(function () {
    'use strict';
    var app = angular.module('app');

    app.filter('docType', function(Config) {
        var types = Config.fileTypes;

        return function(type){
            for(var typeItem in types) {
                if(types.hasOwnProperty(typeItem) && ~types[typeItem].indexOf(type)) {
                    return typeItem;
                }
            }
        }
    })
})();

