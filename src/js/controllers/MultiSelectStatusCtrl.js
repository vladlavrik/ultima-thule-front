(function () {
    'use strict';
    const app = angular.module('app');
    app.controller('MultiSelectStatus', ['$rootScope', '$scope',
        ($rootScope, $scope) => {

            let selected = $scope.state.selected || [];

            $scope.$watch('state.selected', update);


            $scope.selectAll = function () {
                $rootScope.$emit('select-all');
                update();
            };

            $scope.cancel = function () {
                $rootScope.$emit('select-cancel');
                update();
            };


            function update() {
                selected = $scope.state.selected || [];
                $scope.count = selected.length < 100
                    ? selected.length
                    : '...';

                $scope.hasFolders = selected.some(item => item.isFolder);

            }

        }
    ]);
})();