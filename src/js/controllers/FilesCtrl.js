(function () {
	'use strict';
	const app = angular.module('app');

	app.controller('FilesCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$element', 'Api', 'Config',
		($rootScope, $scope, $state, $stateParams, $element, Api, Config) => {

			let folderId = $stateParams['folderId'] || 'root';
			let {listsSort} = $scope.state.uiSettings;
			let {fileTypes} = Config;

			
			/*
			 * Get data from server
			 * and apply for scope
			 */
			Api.folder.get({id: folderId}).$promise.then(resp => {
				if(resp.error) {/*todo ui error show*/
					return;
				}

				let {property, content: {folders, files}} = resp.result;

				// set more attributes
				for(let file of folders) {
					file.isFolder = true;
				}

				for(let file of files) {
					let types = Object.keys(fileTypes);
					let docType = types.find(item => ~fileTypes[item].indexOf(file.extension));
					file.docType = docType || 'unknown';
				}

				$scope.state.curFolder = property;
				$scope.list = [...folders, ...files];
				$scope.list.sort(compareList)

			}, err => {
				/*todo ui error show*/});



			function compareList(a, b) {
				if(a.isFolder !== b.isFolder) {
					return !a.isFolder - !b.isFolder;
				}
				let prop = listsSort;
				let aProp = typeof a[prop] === "string" ? a[prop].toLowerCase() : a[prop];
				let bProp = typeof b[prop] === "string" ? b[prop].toLowerCase() : b[prop];
				let aName = a['name'].toLowerCase();
				let bName = b['name'].toLowerCase();
				let aTime = a['create_time'];
				let bTime = b['create_time'];

				return (prop in a && prop in b ? (aProp > bProp) - (bProp > aProp) : 0)
					|| (bName < aName) - (aName < bName)
					|| (bTime < aTime) - (aTime < bTime);
			}




			/*
			 * Update list after
			 * change sort settings
			 */
			$scope.$watch('state.uiSettings.listsSort', (newValue, oldValue) => {
				if(newValue !== oldValue) {
					$scope.list.sort(compareList);
				}
			});

			/*
			 * ???
			 * ???
			 */
			$rootScope.$on('select-all', () => {
				for(let item of $scope.list) {
					item.selected = true;
				}
				$scope.state.selected = $scope.list.slice();
			});

			$rootScope.$on('select-cancel', () => {
				for(let item of $scope.list) {
					item.selected = false;
				}
				$scope.state.selected = [];
			});






			/*
			 * show preview and open/view
			 * items handler
			 */

			$scope.select = () =>  {
				$scope.state.selected = $scope.list.filter(item => item.selected);
			};

			$scope.preview = (item, event) =>  {
				let metaKey = event.metaKey || event.ctrlKey;
				if(item.isFolder && metaKey) {
					return; // navigate to new window
				}

				event.preventDefault();

				$rootScope.$emit('show-preview', item);
			};
			
			
			$scope.view = function (item, event) {
				
				if(item.isFolder) {
					$state.go('main.files', {folderId: item.id});
					return;
				}

				$rootScope.$emit('viewer', item, $scope.list);
			};


			$scope.download = function () {};

			$scope.rename = function (item) {
				item.rename = true;
			};

			$scope.move = function () {};

			$scope.copy = function () {};
			
			$scope.remove = function () {};

			$scope.done = function (item) {
				item.rename = false;
			};

			$scope.cancel = function (item) {
				item.rename = false;
			};





			/*
			 * Start delegate events for:
			 *  - select
			 *  - viewed/open
			 *  - run action
			 */
			//var rootElement = $element[0].querySelector('.files');
			//var lastClicked;
			//var clickTm;
            //
			//rootElement.addEventListener('click', delegateEventHandler);
            //
			//function delegateEventHandler(e) {
			//	var target = e.target;
			//	while(!target.href && !target.dataset.act) {
			//		if(target === rootElement) {
			//			return;
			//		}
			//		target = target.parentNode;
			//	}
            //
			//	e.preventDefault();
            //
			//	var metaKey = e.ctrlKey || e.metaKey;
			//	var act = target.dataset.act;
			//	var isFolder = target.dataset.isFolder === 'true';
			//	var itemId = parseInt(target.dataset.itemId);
			//	var item, i;
			//	for(i in $scope.list) if($scope.list.hasOwnProperty(i)) {
			//		item = $scope.list[i];
			//		if(!!item.isFolder == isFolder &&  item.id == itemId) {
			//			break;
			//		}
			//	}
            //
			//	if(act) {
			//		itemAction(item, metaKey, act);
			//		return;
			//	}
            //
			//	if(lastClicked === target) {
			//		itemViewed(item, metaKey);
			//		clearTimeout(clickTm);
			//		return;
			//	}
            //
			//	lastClicked = target;
			//	clickTm = window.setTimeout(itemSelect, DB_CLICK_INTERVAL, item, metaKey);
			//}
            //
            //
			//// run action for item
			//function itemAction(item, metaKey, act) {
			//	lastClicked = null;
			//	console.log('run action ' + act) ;
            //
			//}
            //
            //
			//// viewed file or go to folder
			//function itemViewed(item, metaKey) {
			//	lastClicked = null;
			//	if(item.isFolder) {
			//		$state.go('main.files', {folderId: item.id});
			//		return;
			//	}
			//	var list = $scope.list.filter(function (curItem) {
			//		return curItem['type'] === item['type']
			//	});
            //
			//	switch (item.type) {
			//		case 'audio':
			//			$rootScope.$emit('playAudio', item, list);
			//			break;
			//		case 'video':
			//			// либо переходить на новый стейт или открывать свернутое видео в этом же стейте
			//			break;
			//		case 'image':
			//			// перейти на стейт с вьювером
			//			break;
			//		case 'document':
			//			// перейти на стейт с вьювером
			//			break;
			//	}
			//}
            //
            //
			//function itemSelect(item, metaKey) {
			//	lastClicked = null;
            //
			//	var curSelect = $scope.list.filter(function (item) {
			//		return item.selected;
			//	});
            //
			//	if(!metaKey) {
			//		curSelect.forEach(function (item) {
			//			item.selected = false;
			//		});
			//	}
			//	item.selected = metaKey ? !item.selected : true;
            //
			//	if(curSelect.length === 1 && !item.selected) {
			//		document.removeEventListener('click', clearSelected);
			//	} else if(!curSelect.length) {
			//		document.addEventListener('click', clearSelected);
			//	}
            //
			//	$scope.$apply('list');
			//}
            //
            //
			//function clearSelected(e) {
			//	var target = e.target;
			//	while(target !== document.body) {
			//		if(target.classList.contains('files')) {
			//			return;
			//		}
			//		target = target.parentNode;
			//	}
			//	document.removeEventListener('click', clearSelected);
			//	e.preventDefault();
			//	e.stopPropagation();
            //
			//	$scope.list.forEach(function (item) {
			//		item.selected = false;
			//	});
			//	$scope.$apply();
			//}
            //
			//$scope.$watch('list', function (value) {
			//	if(!value) {
			//		$scope.state.selected = [];
			//		return;
			//	}
			//	$scope.state.selected = value.filter(function (item) {
			//		return item.selected;
			//	});
			//}, true);





			/*
			 * Clean after destroy
			 *  - events
			 *  - curFolder
			 */
			$scope.$on('$destroy', function () {
				//rootElement.removeEventListener('click', delegateEventHandler);
				//todo clear state.selected
				$scope.state.curFolder = null;
			});

		}
		//todo: clean watchers

		// select - моментально по клику, превью на сайдбаре псле таймера,
		// при втором клике(переход) снимать таймер,
		// снимать селект при двойном клике на другом файле
		// при драгндропе выставлять высотуц отступа высотой скролла
		// при драгндропе контролы выделенного обекта идут поверх
	]);
})();