(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('AppCtrl', function ($rootScope, $scope, Person) {

		checkUser();

		$rootScope.checkUser = checkUser;

		function checkUser() {
			$rootScope.currentUser = undefined;

			Person.get().then(function (user) {
				$rootScope.currentUser = user;
				$rootScope.$emit('loginSuccess');
			}).catch(function (error) {
				console.warn('Auth Error:' + error);
				$rootScope.currentUser = null;
				$rootScope.$emit('loginFailed');
			});
		}
	});
})();