(function () {
    'use strict';
    var app = angular.module('app');
    app.controller('InfoBar', ['$rootScope', '$scope', 'Upload', function ($rootScope, $scope, Upload) {

        var infoShowTime = 4000;
        var infoHideTm;

        function hideInfo() {
            $scope.state.upload = null;
            $scope.$apply('state.upload');
        }
        

        Upload.on('start',  function() {
            clearTimeout(infoHideTm);
            $scope.state.upload = {
                status: 'during',
                progress: 0
            };
            $scope.$apply('state.upload');
        });


        Upload.on('progress', function(loaded, total) {
            $scope.state.upload.progress = loaded / total * 100;
            $scope.$apply('state.upload.progress');
        });


        Upload.on('success', function () {
            $scope.state.upload.status = 'success';
            $scope.$apply('state.upload.status');
            infoHideTm = setTimeout(hideInfo, infoShowTime);
        });


        Upload.on('error', function(error, code, msg) {
            var errorName;

            if(error === 'file_limit') {
                errorName = 'error_file_limit';
            } else if(error === 'disk_limit') {
                errorName = 'error_user_limit';
            } else {
                errorName = 'error_unknown';
            }

            $scope.state.upload.status = errorName;
            $scope.$apply('state.upload.status');
            infoHideTm = setTimeout(hideInfo, infoShowTime);
        });

        // todo add abort upload
    }]);
})();