(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('AuthCtrl', ['$scope', '$state', '$stateParams', function ($scope, $state, $stateParams) {
		var curView = $stateParams['view'];

		if(!~['signin', 'signup', 'recovery', 'recovery-finish'].indexOf(curView)) {
			$state.go('auth', {view: null});
		}

		$scope.current = curView

	}]);
})();