(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('ViewerCtrl', ['$scope',
		function ($scope) {
			console.log('ViewerCtrl');
			$scope.$on('$destroy', function () {
				console.log('destroy ViewerCtrl');
			})
		}

	]);
})();