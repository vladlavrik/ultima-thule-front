(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('AuthSignInCtrl', ['$rootScope', '$scope', '$state', 'Api', function ($rootScope, $scope, $state, Api) {

		$scope.values = {};
		$scope.error = null;

        $scope.$watch('[values.login, values.password]', function () {
            $scope.error = null;
        });

		$scope.submit = function () {
			if($scope.sending) {
				return;
			}

            $scope.sending = true;
            $scope.error = null;

            var data = {
				login: $scope.values.login,
				password: $scope.values.password
			};

            Api.signIn.save(data).$promise.then(function (resp) {
                $scope.sending = null;

                var error = resp.error;
				if (error) {
					$scope.error = true;
                    return;
				}

                // update user state
                $rootScope.checkUser();

                // redirect to main page
                $state.go('main.files');

            }, function (err) {
                $scope.sending = null;
                $scope.error = true;
			});
		};

	}]);
})();