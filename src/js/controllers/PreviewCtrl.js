(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('PreviewCtrl', ['$rootScope', '$scope',
		($rootScope, $scope) => {

			$rootScope.$on('show-preview', (e, item) => {
				$scope.item = item;
			});

			$rootScope.$on('hide-preview', () => {
				$scope.item = null;
			});

			$rootScope.$on('$stateChangeSuccess', () => {
				$scope.item = null;
			});
		}
	]);
})();