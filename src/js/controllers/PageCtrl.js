(function () {
	'use strict';
	var app = angular.module('app');

	app.controller('PageCtrl', ['$scope',  '$rootScope', 'Upload', function ($scope,  $rootScope, Upload) {

		var uiSettings =  $rootScope.currentUser['ui_settings'];

		$scope.state = {
			uiSettings: uiSettings,
			listsSortOptions: [
				{name: 'по имени', value:'name' },
				{name: 'по размеру', value:'size' },
				{name: 'по типу', value:'type' },
				{name: 'по дате загрузки', value:'create_time' }
			]
		};

		$scope.Upload = Upload;

	}]);

	/*
	* TODO
	* on change location:
	* state.showNav = false
	* state.showAside = false
	* hide all dd
	*
	* on start upload on start folder create:
	* hide create dd
	* */
})();