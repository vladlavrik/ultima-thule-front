(function () {
	'use strict';
	const app = angular.module('app');

	app.controller('AudioPlayer', ['$rootScope', '$scope', 'Audio', 'Config',
		($rootScope, $scope, Audio, Config) => {

			let formats = Config.fileTypes.audio;

			Audio.setOptions({
				srcDir: '/filestorage/'
			});
			


			$rootScope.$on('viewer', (event, item, list) => {
				if(formats.indexOf(item.extension) === -1){
					return;
				}
				// check support format and show error
				
				let playlist = list.filter(item => ~formats.indexOf(item.extension));
				let index = playlist.indexOf(item);


				$scope.item = item;
				$scope.list = playlist;
				$scope.name = item.name;
				$scope.title = item.description.title;
				$scope.artist = item.description.artist;

				$scope.isShow = true;

				playlist = playlist.map(item => item.path_file);

				Audio.init(playlist);
				Audio.play(index);
				
			});

			$scope.$watch('state.uiSettings.soundVolume', (value) => {
				Audio.setVolume(value / 100);
			});


			// controls play
			$scope.pause = Audio.pause;
			$scope.preload = 0;
			$scope.progress = {
				time: 0,
				completed: 0
			};

			Audio.on('paused', paused => {
				$scope.paused = paused;
			});

			$scope.rewind = position => Audio.setPosition(position, 100);


			// progress and preload values
			Audio.on('progress', (completed, time) => {
				$scope.progress = {completed, time};
				$scope.$apply('progress');
			});

			Audio.on('preload', preload => {
				$scope.preload = preload;
				$scope.$apply('preload');
			});



			// frequency values
			Audio.on('frequency', frequency => {
				$scope.frequency = frequency;
				$scope.$apply('frequency');
			});


			// equalizer
			$scope.equalizer = [0,0,0,0,0,0,0,0,0,0];
			$scope.equalizerSet = [];

			$scope.equalizer.map((item, num) => {
				$scope.equalizerSet[num] = (a) => Audio.equalizer = $scope.equalizer;
			});




		}

		//todo ошибка если файл воспроизвести невозможно
	]);
})();