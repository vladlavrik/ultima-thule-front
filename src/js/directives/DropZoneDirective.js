(function () {
    'use strict';
    var app = angular.module('app');

    //dragenter dragover dragleave drop

    app.directive('dropzone', [
        function () {
            return {
                restrict: 'EA',
                transclude: true,
                scope: {
                    handler: '=dropzoneHandler',
                    addData: '=dropzoneData'
                },
                replace: true,
                templateUrl: 'dropzone.html',
                link: function ($scope, elem) {

                    var timeout;
                    document.ondragleave = function (e) {
                        clearTimeout(timeout);
                        timeout = setTimeout(function() {
                            $scope.showDropZone = false;
                            $scope.$apply('showDropZone');
                        }, 200);
                    };

                    document.ondragover = function (e) {
                        clearTimeout(timeout);
                        e.preventDefault();
                        $scope.showDropZone = true;
                        $scope.$apply('showDropZone');
                    };

                    document.ondrop = function (e) {
                        $scope.showDropZone = false;
                        $scope.$apply('showDropZone');

                        var target = e.target;
                        while (target !== elem[0]) {
                            if(!target) return;
                            target = target.parentNode;
                        }
                        e.preventDefault();

                        var files = e.dataTransfer.files;

                        if(files.length) {
                            $scope.handler(files, $scope.addData || {});
                        }
                    };
                }
            };
        }
    ]);


})();