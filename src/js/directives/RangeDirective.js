(function () {
	'use strict';
	const app = angular.module('app');


	app.directive('range', [
        () => {
			return {
				restrict: 'EA',
				scope: {
					value: '=rangeValue',
                    shadowValue: '=rangeShadowValue',
                    min: '=rangeMin',
                    max: '=rangeMax',
                    step: '=rangeStep',
                    finalOnly: '@rangeFinalOnly',
                    handler: '=rangeHandler',
                    vertical: '@rangeVertical',
                    editable: '@rangeEditable',
                    bullet: '@rangeBullet'
				},
				replace: true,
				templateUrl: 'range.html',

                link ($scope, element) {
                    let {
                        value = 0,
                        shadowValue,
                        min = 0,
                        max = 100,
                        step,
                        finalOnly,
                        handler,
                        vertical,
                        editable
                    } = $scope;

                    let baseEl = element[0];
                    let length = max - min;

                    $scope.length = length;


                    /*
                     * Update styles methods
                     */
                    $scope.sizeStyle = value => new Object({
                        [vertical ? 'height' : 'width']: value + '%'
                    });
                    
                    $scope.positionStyle = value => new Object({
                        [vertical ? 'bottom' : 'left']: value + '%'
                    });


                    /*
                     * Set update handlers
                     */
                    let listenerComplete;

                    let toComplete = value => Math.floor((value - min) / length * 100);

                    let setComplete = value => {
                        $scope.complete = toComplete(value);
                    };

                    let startListenComplete = () => {
                        listenerComplete = $scope.$watch('value', setComplete);
                    };

                    let stopListenComplete = () => {
                        listenerComplete();
                    };


                    // watch main complete
                    startListenComplete();

                    // watch shadow complete
                    $scope.$watch('shadowValue', value => {
                        $scope.shadowComplete = toComplete(value);
                    });




                    /*
                     * Set editable handlers
                     */
                    let startChange = e => {
                        e.preventDefault();
                        document.addEventListener('mousemove', stepChange);
                        document.addEventListener('mouseup', stopChange);

                        if(finalOnly) {
                            stopListenComplete();
                        }
                        updateValue(e.clientX, e.clientY);
                    };

                    let stepChange = e => {
                        e.preventDefault();
                        updateValue(e.clientX, e.clientY);
                    };

                    let stopChange = e => {
                        document.addEventListener('mousemove', stepChange);
                        document.removeEventListener('mousemove', stepChange);
                        document.removeEventListener('mouseup', stopChange);

                        if(finalOnly) {
                            startListenComplete();
                        }

                        let value = calculateValue(e.clientX, e.clientY);
                        if (value === $scope.value) {
                            return;
                        }
                        $scope.value = value;
                        $scope.$apply('value');
                        if(handler) {
                            handler(value);
                        }
                    };

                    let updateValue = (posX, posY) => {
                        let value = calculateValue(posX, posY);

                        if(finalOnly) {
                            setComplete(value);
                            $scope.$apply('complete');
                            return;
                        }

                        $scope.value = value;
                        $scope.$apply('value');
                        if(handler) {
                            handler(value);
                        }
                    };


                    let calculateValue = (posX, posY) => {
                        let baseElPos = baseEl.getBoundingClientRect();

                        let absValue = vertical
                            ? -((posY - baseElPos.top) / baseEl.clientHeight * length - length)
                            : (posX - baseElPos.left) / baseEl.clientWidth * length;


                        let value = absValue + min;

                        let residueMax = Math.abs(max - value);
                        let residueMin = Math.abs(min - value);
                        let residue = Math.min(residueMin, residueMax);

                        if (step && step / 2 > residue) {
                            return absValue > length / 2 ? max : min;
                        }

                        value = step ? Math.round(value / step) * step : value;
                        value = Math.min(value, max);
                        value = Math.max(value, min);
                        return value;
                    };


                    if(editable) {
                        baseEl.addEventListener('mousedown', startChange);
                    }




                    /*
                     * Set $destroy handler
                     */
                    $scope.$on('$destroy', () => {
                        stopChange();
                        baseEl.removeEventListener('mousedown', startChange);
                    });
                }

                //todo optimization: no repeat trigger 'complete' update and 'value' update with same value
			};
		}
	]);

})();