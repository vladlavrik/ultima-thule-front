(function () {
    'use strict';
    var app = angular.module('app');


    app.directive('dropdown', [
        function () {
            var currentExpandedScope = null;

            function hideCurrent () {
                if(currentExpandedScope) {
                    currentExpandedScope.isExpanded = false;
                }
                currentExpandedScope = null;
            }

            return {
                restrict: 'EA',
                transclude: true,
                scope: {
                    type: '@dropdown',
                    label: '@dropdownLabel',
                    labelDefault: '@dropdownDefaultLabel',
                    rightAlign: '@dropdownRightAlign',
                    model: '=dropdownSelectModel',
                    selected: '=dropdownSelected'
                },
                replace: true,
                templateUrl: 'dropdown.html',
                link: function ($scope, element, attrs) {

                    var labelChanges = !$scope.label;
                    var labelEl = element[0].querySelector('.dropdown-label');

                    // expanded content
                    $scope.isExpanded = false;
                    labelEl.addEventListener('click', expand);


                    // label update in select
                    if($scope.type === 'select') {
                        $scope.onSelect = select;

                        if($scope.selected) {
                            select($scope.selected);
                        }
                    }

                    function expand (e) {
                        var isExpanded = $scope.isExpanded;
                        e.preventDefault();
                        hideCurrent();
                        $scope.isExpanded = !isExpanded;
                        if($scope.isExpanded) {
                            currentExpandedScope = $scope;
                        }
                        $scope.$apply();
                    }

                    function select(selectedValue) {
                        var list = $scope.model;
                        var selected;
                        for(var i=0; i < list.length; i++) {
                            if(list[i].value === selectedValue) {
                                selected = list[i];
                                break;
                            }
                        }
                        $scope.selected = selected.value;
                        if(labelChanges) {
                            $scope.label = selected.name;
                        }
                        $scope.isExpanded = false;
                    }
                }
            };
        }
    ]);


    //todo: on update event, on hover expand type, destroy, delegate events click, multiselect
})();