(function () {
	'use strict';
	var app = angular.module('app');


	app.directive('frequency', [
		function () {
			return {
				restrict: 'EA',
				scope: {
					value: '=frequencyValue',
                    width: '@frequencyWidth',
                    height: '@frequencyHeight',
                    color: '@frequencyColor'
				},
                templateUrl: 'frequency.html',
                link: function ($scope, elem) {
                    var width = parseInt($scope.width);
                    var height = parseInt($scope.height);
                    var color = $scope.color;
                    var ctx = elem.find('canvas')[0].getContext('2d');

                    $scope.$watch('value', function (value) {
                        if(!value) {
                            return;
                        }
                        ctx.clearRect(0, 0, width, height);
                        ctx.fillStyle =color;
                        ctx.strokeStyle = color;
                        ctx.beginPath();
                        ctx.moveTo(0, height);
                        var i, x, y;
                        var ratio = width / value.length ;
                        for(i = 0; i < value.length; i++) {
                            x = i * ratio + 1;
                            y = height - value[i] / 256 * height;
                            ctx.lineTo(x, y);
                        }
                        ctx.stroke();
                    }, true);

                    $scope.$on('$destroy', function () {});
                }

            };
        }
	]);

})();