'use strict';
const path = require('path');
const gulp = require('gulp');
const debug = require('gulp-debug');
const newer = require('gulp-newer');
const remember = require('gulp-remember');
const gulpif = require('gulp-if');
const concat = require('gulp-concat');
const sourceMaps = require('gulp-sourcemaps');
const inject = require('gulp-inject');
const del = require('del');
const svgSprite = require('gulp-svg-sprite');
const stylus = require('gulp-stylus');
const jade = require('gulp-jade');
const htmlify = require('gulp-angular-htmlify');
const templateCache = require('gulp-angular-templatecache');
const mainBowerFiles = require('main-bower-files');
const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
//const liveReload = require('gulp-livereload');



/*
 * Set configuration values
 */
const tmpDir = process.cwd() + '/tmp/';

const config = {
    images: {
        src: ['src/img/**/*.{svg,png,gif,jpg}', '!src/img/sprite/**'],
        basepath: 'src/img',
        dest: 'build/img'
    },
    sprite: {
        src: 'src/img/sprite/**/*.svg',
        stylSrc: 'sprite.svg',
        stylDest: 'sprite.styl',
        prefix: '$icon-',
        dest: 'build/img'
    },
    styles: {
        src: 'src/styl/main.styl',
        allSrc: 'src/styl/**/*.styl',
        resolverFrom: /^(\.\.\/|\/)?(img\/)?/,
        resolverTo: '/img/',
        dest: 'build/css'
    },
    templates: {
        src: ['src/views/**/*.jade', '!src/views/index.jade'],
        customPrefixes: ['ui', 'dropdown', 'dropzone', 'range', 'frequency'],
        dest: 'build/js/'
    },
    scripts: {
        existing: 'build/js/compiled.**',
        src: [
            'src/js/config.js',
            'src/js/app.js',
            'src/js/**/*.js'
        ],
        output: 'compiled.js',
        outputMin: 'compiled.min.js',
        dest: 'build/js'
    },
    components: {
        existing: 'build/js/external.**',
        src: 'bower_components',
        output: 'external.js',
        outputMin: 'external.min.js',
        dest: 'build/js'
    },
    index: {
        sources: ['build/js/{external,compiled,templates}.{js,min.js}', 'build/css/*.css'],
        src: 'src/views/index.jade',
        dest: 'build'
    }
};

/*
 * Set "developer" of "production" state
 */
let isDev = process.env.NODE_ENV !== 'production';





/*
 * Tasks:
 *
 * Builder:
 * gulp build:images
 * gulp build:sprite
 * gulp build:styles
 * gulp build:templates
 * gulp build:scripts
 * gulp build:components
 * gulp build:index
 *
 * Cleaner:
 * gulp clean
 *
 * Main:
 * gulp build > build project
 * gulp dev   > build project and start watch
 *
 * Default    > dev
 */

gulp.task('build:images', function() {
    return gulp.src(config.images.src)
        .pipe(newer(config.images.dest))
        .pipe(gulp.dest(config.images.dest));
});


gulp.task('build:sprite', function() {
    let conf = config.sprite;
    return gulp
        .src(conf.src)
        .pipe(svgSprite({
            shape: {
                spacing: { padding: 0 }
            },
            mode: {
                css: {
                    dest: '.',
                    prefix: conf.prefix,
                    sprite: conf.stylSrc,
                    dimensions: true,
                    bust: !isDev,
                    render: {
                        styl: {
                            dest: conf.stylDest
                        }
                    }
                }
            }
        }))
        .pipe(gulpif('*.styl', gulp.dest(tmpDir), gulp.dest(conf.dest)));
});


gulp.task('build:styles', function() {
    let conf = config.styles;
    return gulp.src(conf.src)
        .pipe(stylus({
            import: tmpDir + config.sprite.stylDest,
            define: {
                url: file => {
                    let url = file.val.replace(conf.resolverFrom, conf.resolverTo);
                    file.val = `url(${url})`;
                    file.quote = '';
                    return file;
                }
            }
        }))
        .pipe(gulpif(!isDev, cssmin()))
        .pipe(gulp.dest(conf.dest));
});


gulp.task('build:templates', function() {
    let conf = config.templates;
    let customPrefixes = config.templates.customPrefixes;
    return gulp.src(conf.src, {since: gulp.lastRun('build:templates')})
        .pipe(jade({
            pretty: true
        }))
        .pipe(htmlify({customPrefixes}))
        .pipe(remember('templates'))
        .pipe(templateCache({
            module: 'app'
        }))
        .pipe(gulp.dest(conf.dest));
});


gulp.task('build:scripts', function() {
    let conf = config.scripts;
    return gulp.src(conf.src, {since: gulp.lastRun('build:scripts')})
        .pipe(remember('scripts'))
        .pipe(gulpif(isDev, sourceMaps.init()))
        .pipe(concat(conf.output))
        .pipe(gulpif(!isDev, uglify({mangle: true})))
        .pipe(gulpif(isDev, sourceMaps.write('.')))
        .pipe(gulp.dest(conf.dest));
});


gulp.task('build:components', function() {
    let conf = config.components;
    return gulp.src(mainBowerFiles())
        .pipe(concat(conf.output))
        .pipe(gulpif(!isDev, uglify({mangle: true})))
        .pipe(gulp.dest(conf.dest));
});


gulp.task('build:index', function() {
    let conf = config.index;
    let customPrefixes = config.templates.customPrefixes;
    let sources = gulp.src(conf.sources, { read:false });

    return gulp.src(conf.src)
        .pipe(jade({
            pretty: true
        }))
        .pipe(htmlify({customPrefixes}))
        .pipe(inject(sources, {
            transform: function () {
                let args = arguments;
                args[0] = args[0].replace(/\/build\//, '\/');
                return inject.transform(...args);
            },
            removeTags: true
        }))
        .pipe(gulp.dest(conf.dest));
});






/*
 * Cleaner
 */
gulp.task('clean', function() {
    return del(['build', tmpDir]);
});





/*
 * Watcher
 */
gulp.task('watch', function() {

    gulp.watch(config.images.src, gulp.series('build:images'))
        .on('unlink', file => del(file.replace(config.images.basepath, config.images.dest)));

    gulp.watch(config.sprite.src, gulp.series('build:sprite'));

    gulp.watch([config.styles.allSrc, tmpDir + config.sprite.stylDest], gulp.series('build:styles'));

    gulp.watch(config.templates.src, gulp.series('build:templates'))
        .on('unlink', file => remember.forget('templates', path.resolve(file)));

    gulp.watch(config.scripts.src, gulp.series('build:scripts'))
        .on('unlink', file => remember.forget('scripts', path.resolve(file)));

    gulp.watch(config.components.src, gulp.series('build:components'));

    gulp.watch(config.index.src, gulp.series('build:index'));
});



/*
 * Main builders
 */

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('build:images', 'build:sprite', 'build:templates', 'build:scripts', 'build:components'),
    'build:styles',
    'build:index'
));


gulp.task('default', gulp.series('build'));